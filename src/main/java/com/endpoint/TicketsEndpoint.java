package com.endpoint;

import com.seminarka.jira.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.service.TicketService;

@Endpoint
public class TicketsEndpoint {

    private static final String NAMESPACE_URI = "http://seminarka.com/jira";

    @Autowired
    TicketService ticketService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTicket")
    @ResponsePayload
    public GetTicketResponse getTicketNameResponse(@RequestPayload GetTicket getTicket){
        GetTicketResponse response = new GetTicketResponse();
        response.setTicket(ticketService.getTicketByName(getTicket.getName()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTicketId")
    @ResponsePayload
    public GetTicketResponse getTicketIdResponse(@RequestPayload GetTicketId getTicketId){
        GetTicketResponse response = new GetTicketResponse();
        response.setTicket(ticketService.getTicketById(getTicketId.getId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addTicket")
    @ResponsePayload
    public AddTicketResponse addTicket(@RequestPayload AddTicketRequest addTicketRequest) {
        AddTicketResponse response = new AddTicketResponse();
        response.setTicket(ticketService.saveTicket(addTicketRequest.getTicket()));
        return response;

    }

}
