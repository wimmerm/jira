package com.service;


import com.repository.TicketRepository;
import com.seminarka.jira.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;


@Service
public class TicketService {



    @Autowired
    TicketRepository ticketRepository;

    @PostConstruct
    public void initializeData(){

        Ticket ticket1 = new Ticket();
        ticket1.setName("test");
        ticket1.setEmail("test@test.cz");
        ticket1.setIdPersonCreator(2);
        ticket1.setIdPersonAssigned(4);
        ticket1.setCreationDatetime(LocalDateTime.now().toString());
        ticket1.setTicketCloseDatetime(LocalDateTime.now().plusMonths(1).toString());

        Ticket ticket2 = new Ticket();
        ticket2.setName("test2");
        ticket2.setEmail("test@test.cz");
        ticket2.setIdPersonCreator(23);
        ticket2.setIdPersonAssigned(24);
        ticket2.setCreationDatetime(LocalDateTime.now().toString());
        ticket2.setTicketCloseDatetime(LocalDateTime.now().plusMonths(1).toString());

        Ticket ticket3 = new Ticket();
        ticket3.setName("test3");
        ticket3.setEmail("test@test.cz");
        ticket3.setIdPersonCreator(23);
        ticket3.setIdPersonAssigned(64);
        ticket3.setCreationDatetime(LocalDateTime.now().toString());
        ticket3.setTicketCloseDatetime(LocalDateTime.now().plusMonths(1).toString());


        ticketRepository.save(ticket1);
        ticketRepository.save(ticket2);
        ticketRepository.save(ticket3);
    }

    public Ticket getTicketByName(String name){
        return ticketRepository.findAllByName(name);
    }

    public Ticket getTicketById(int id){
        return ticketRepository.findAllById(id);
    }

    public Ticket saveTicket(Ticket ticket){
        return ticketRepository.save(ticket);
    }

}
