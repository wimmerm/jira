package com.repository;

import com.seminarka.jira.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket,Integer> {

    Ticket findAllById(Integer id);
    Ticket findAllByName(String name);

}
